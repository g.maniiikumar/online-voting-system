package com.project.ovs.dto;

import com.project.ovs.entity.StudentReport;

public class StudentReportResponse {

private	Result result;
private StudentReport studentreport;


public Result getResult() {
	return result;
}
public void setResult(Result result) {
	this.result = result;
}
public StudentReport getStudentreport() {
	return studentreport;
}
public void setStudentreport(StudentReport studentreport) {
	this.studentreport = studentreport;
}
@Override
public String toString() {
	return "StudentReportResponse [result=" + result + ", studentreport=" + studentreport + "]";
}
public StudentReportResponse(Result result, StudentReport studentreport) {
	super();
	this.result = result;
	this.studentreport = studentreport;
}

public StudentReportResponse(Result result) {
	super();
	this.result = result;

}


public StudentReportResponse() {
	super();
	// TODO Auto-generated constructor stub
}


	
}
