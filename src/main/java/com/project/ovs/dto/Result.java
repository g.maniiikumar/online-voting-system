package com.project.ovs.dto;

public class Result {

	private int status;
	private String message;
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "Result [status=" + status + ", message=" + message + "]";
	}
	public Result(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	public Result() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
