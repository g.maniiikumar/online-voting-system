package com.project.ovs.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import com.project.ovs.Utils.OVSConstants;
import com.project.ovs.dto.Result;
import com.project.ovs.dto.StudentReportResponse;
import com.project.ovs.entity.Nominations;
import com.project.ovs.entity.StudentInfo;
import com.project.ovs.entity.StudentReport;
import com.project.ovs.repository.NominationsRepo;
import com.project.ovs.repository.StudentPercentageRepo;
import com.project.ovs.repository.StudentProfileRepo;
import com.project.ovs.service.StudentService;


@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentPercentageRepo studentpercentagerepo;
	
	@Autowired
    private StudentProfileRepo studentprofilerepo;
	
	@Autowired
	private NominationsRepo nominationsrepo;
	
	
	@Override
	public StudentReportResponse getStudentReport(String studid) {

		Result result = new Result();
		
		Optional<StudentReport> student_report = studentpercentagerepo.findById(studid);

		if (!student_report.isPresent()) {
			result.setStatus(4);
			result.setMessage("data not found while fetching from server");
			return new StudentReportResponse(result);
		}  else {
			result.setStatus(1);
			result.setMessage("successfully fetched the student report");
			return new StudentReportResponse(result, student_report.get());
		}

	}

	@Override
	public String saveStudent(StudentInfo studentinfo) {
    String filePath=OVSConstants.Folder_path+studentinfo.getFile().getOriginalFilename();
             StudentInfo studentInfo=studentprofilerepo.save(studentinfo.builder()
    		.img_name(studentinfo.getFile().getOriginalFilename())
    		.img_type(studentinfo.getFile().getContentType())
    		.imag_filePath(filePath)
    		.studentName(studentinfo.getStudentName())
    		.studid(studentinfo.getStudid())
    		.mobile(studentinfo.getMobile())
    		.address(studentinfo.getAddress())
    		.email(studentinfo.getEmail())
    		.branch(studentinfo.getBranch())
    		.build());
    
    try {
		studentinfo.getFile().transferTo(new File(filePath));
		if(studentInfo!=null) {
			return "file uploaded successfully:" + filePath;
		}
		
	} catch (IllegalStateException | IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return null;
    
    
    
   


		
	}

	

//	@Override
//	public ResponseEntity<?> sendNomination(String studid) {
//		StudentReportResponse response=getStudentReport(studid);
//		StudentReport report=response.getStudentreport();
//		
//			if(report==null) {
//				
//			response.setResult(new Result(2, "check studentid"));
//				return ResponseEntity.ok(response.getResult());
//			}
//			
//			else if(response.getStudentreport().getPercentage()>=75.0) {
//				response.setResult(new Result(1, "you are eligible to nominate"));
//				return ResponseEntity.ok(response.getResult());
//			}
//			else {
//		         response.setResult(new Result(3,"you are not eligible to nominate"));
//		         return ResponseEntity.ok(response.getResult());
//			}
//	}
	
	
	
	@Override
	public Result sendNomination(String studid) {
		StudentReportResponse response=getStudentReport(studid);
		
		StudentReport studentreport=response.getStudentreport();
		Nominations nominations=new Nominations();
		if(studentreport==null) {
		response.setResult(new Result(3, "check student id or student id wrong"));
		return response.getResult();
		}
		else if(studentreport.getPercentage()>=75.0) {
			nominations.setStudid(studid);
			nominations.setStatus("nominated");
			Nominations nomination=nominationsrepo.save(nominations);
			return new Result(1, "The nomination has been submitted for"+ nomination.getStudid());

		}
		else {
	         return new Result(2, "You are not eligible to become a student leader");
		}
		
	}


	public void UpdateNominatedStatus() {
		 Random rand = new Random();
			List<Nominations> nominations=nominationsrepo.findAll();
			int numberOfElements = 3;
		    for (int i = 0; i < numberOfElements; i++) {
			        int randomIndex = rand.nextInt(nominations.size());
			        final Nominations randomElement = nominations.get(randomIndex);
			        nominationsrepo.updateNominationStatus(randomElement.getStudid());
			      
		    }
		    
	}

	@Override
	public List<Nominations> getSelectedNominations() {
		List<Nominations> nominations=nominationsrepo.findAll();
		List<Nominations> selectedNominations;
		List<Nominations> s;
		
	     if(nominations.toString().contains("selected")) {
	    
	    	selectedNominations=nominations.stream().filter(i->i.getStatus().toString().contains("selected")).collect(Collectors.toList());
	    
	    	return selectedNominations; 
	    	}else {
	    	 
	    		UpdateNominatedStatus();
	    	
                s=nominationsrepo.findstatus("selected");
	    		 return s;
			    }
		   
			  //List<Nominations> selectedNominationss=nominationss.stream().filter(j->j.getStatus().toString().contains("selected")).collect(Collectors.toList());
		     
	     }

	
	public Result voteToSelectedStudent(String studpinno, String votepinno) {
		List<StudentInfo> studentinfos=studentprofilerepo.findAll();
		if(studentinfos.stream().anyMatch(o->o.getStudid().equalsIgnoreCase(studpinno))) {
           Optional<StudentInfo> studentinfo=studentprofilerepo.findById(studpinno);
           if(studentinfo.get().getVote()==0) {
        	   int count = 0;
        	   Nominations nominations=nominationsrepo.findBystudid(votepinno);
        	   count=nominations.getVotes_count();
        	   count++;
        	   studentprofilerepo.updatevote(studpinno);
        	   nominationsrepo.updateVotesCount(votepinno, count);
        	   return new Result(1,"Voted! Successfully");
           }else {
        	  return new Result(0,"Sorry! your vote already persisted");
           }
		}else {
			return new Result(2,"Incorrect");
		}
	}

	@Override
	public Result getVoteResult() {
		List<Nominations> selectedNominations=nominationsrepo.findstatus("selected");
		Nominations maxByVotesCount = selectedNominations.stream().max(Comparator.comparing(Nominations::getVotes_count)).orElseThrow(NoSuchElementException::new);
		String msg="The winner is"+" "+ maxByVotesCount.getStudid();
		return new Result(1,msg);
	}
		
	}
