package com.project.ovs.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.ovs.entity.StudentInfo;

@Repository
public interface StudentProfileRepo extends JpaRepository<StudentInfo, String> {

	@Transactional
	@Modifying
	@Query("UPDATE StudentInfo SET vote = 1 WHERE studid = :studpinno")
	void updatevote(String studpinno);

}
