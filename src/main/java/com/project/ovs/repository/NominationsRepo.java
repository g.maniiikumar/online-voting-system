package com.project.ovs.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.ovs.entity.Nominations;

@Repository
public interface NominationsRepo extends JpaRepository<Nominations, Integer>{

	@Transactional
	@Modifying
	@Query("UPDATE Nominations SET status = 'selected' WHERE studid = :studid")
    void updateNominationStatus(String studid);

	//@Query("from Nominations where status= :status")
	//List<Nominations> findAllByStatus(String status);

	
	@Query(
			value="select * from nominations where status=:s",
			nativeQuery=true
			)
	List<Nominations> findstatus(String s);

	
	@Transactional
	@Modifying
	@Query("UPDATE Nominations SET votes_count = :count WHERE studid = :votepinno")
	void updateVotesCount(String votepinno, int count);

	Nominations findBystudid(String votepinno);

	//@Query(value = "SELECT max(votes_count) FROM Nominations")
	@Query(
			value="select * from nominations where max(votes_count)",
			nativeQuery=true
			)
	int maxVoteCount();

	

	

	
	
}
