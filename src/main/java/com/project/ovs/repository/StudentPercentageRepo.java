package com.project.ovs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.ovs.entity.StudentReport;



@Repository
public interface StudentPercentageRepo extends JpaRepository<StudentReport, String>{

}
