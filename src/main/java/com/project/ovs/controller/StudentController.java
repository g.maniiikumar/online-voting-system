package com.project.ovs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.project.ovs.dto.Result;
import com.project.ovs.dto.StudentReportResponse;
import com.project.ovs.entity.Nominations;
import com.project.ovs.entity.StudentInfo;
import com.project.ovs.entity.StudentReport;
import com.project.ovs.service.StudentService;


@RestController
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	
	//Api to find the student report with percentage
	@GetMapping("/get-student-report/{studid}")
	public ResponseEntity<?> getStudentReport(@PathVariable String studid) {
		try {
			StudentReportResponse response=studentService.getStudentReport(studid);
			return ResponseEntity.ok(response);
	}catch(Exception e){
		e.printStackTrace();
		String exceptionMessage = "Some Exception:" + e.getMessage();
		return ResponseEntity.badRequest().body(exceptionMessage);
	}
		
	}
	
	//Api to check the eligiblily to nominate to become a student president
	@ResponseBody
	@GetMapping("/check-eligibility/{studid}")
	public ResponseEntity<?> checkEligibility(@PathVariable String studid) {
		StudentReportResponse response=studentService.getStudentReport(studid);
		StudentReport report=response.getStudentreport();
		try {
			if(report==null) {
				return ResponseEntity.ok("check studentid");
			}
			
			else if(response.getStudentreport().getPercentage()>=75.0) {
				response.setResult(new Result(1, "you are eligible to nominate"));
				return ResponseEntity.ok(response.getResult());
			}
			else {
		         response.setResult(new Result(3,"you are not eligible to nominate"));
		         return ResponseEntity.ok(response.getResult());
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			String exceptionMessage="Some Exception" + e.getMessage();
			return ResponseEntity.badRequest().body(exceptionMessage);
		}
	
	}
	
	@PostMapping("/save-student")
	public String saveStudent(StudentInfo studentinfo) {
		String msg=studentService.saveStudent(studentinfo);
		return msg;
	}
	

	
	@GetMapping("/send-nomination/{studid}")
	public ResponseEntity<Result> nominate(@PathVariable String studid) {
		try {
			Result result=studentService.sendNomination(studid);
			return ResponseEntity.ok(result);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@GetMapping("/get-selected-nominations")
	public List<Nominations> getSelectedNominations() {
		try {
			return studentService.getSelectedNominations();
		}
	catch(Exception e) {
		e.printStackTrace();
	}
		return null;
		
	}
	
	@PostMapping("/vote-selectd-nominee")
	public ResponseEntity<?> voteToSelectedNominee(String studpinno,String votepinno) {
	try {
			Result res=(Result) studentService.voteToSelectedStudent(studpinno,votepinno);
			return ResponseEntity.ok(res);
	}
	catch(Exception e) {
		e.printStackTrace();
	}
	return null;
		
	}
	
	@GetMapping("/get_result")
	public Result getVoteResult() {
		Result res=studentService.getVoteResult();
		return res;
	}
	
}
