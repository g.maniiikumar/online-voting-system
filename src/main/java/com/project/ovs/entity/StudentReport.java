package com.project.ovs.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
@Entity
@Table(name="student_report")
public class StudentReport {
	
	@Id
	@Column(unique=true)
	private String stud_id;
	
	@NotNull
	private String branch;
	
	@NotNull
	private int first_year;
	
	@NotNull
	private int second_year;
	
	@NotNull
	private int third_year;

	@Transient
	private double percentage;
	
	@OneToOne(cascade= CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="studentinfo_id", referencedColumnName="stud_id")
	private StudentInfo studentInfo;

	public String getStud_id() {
		return stud_id;
	}

	public void setStud_id(String stud_id) {
		this.stud_id = stud_id;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public int getFirst_year() {
		return first_year;
	}

	public void setFirst_year(int first_year) {
		this.first_year = first_year;
	}

	public int getSecond_year() {
		return second_year;
	}

	public void setSecond_year(int second_year) {
		this.second_year = second_year;
	}

	public int getThird_year() {
		return third_year;
	}
	
	
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}


	public double getPercentage() {
		return ((first_year+second_year+third_year)/300.0)*100;
	}

	public void setThird_year(int third_year) {
		this.third_year = third_year;
	}

	@Override
	public String toString() {
		return "StudentReport [stud_id=" + stud_id + ", branch=" + branch + ", first_year=" + first_year
				+ ", second_year=" + second_year + ", third_year=" + third_year + "]";
	}

	public StudentReport(String stud_id, String branch, int first_year, int second_year, int third_year) {
		super();
		this.stud_id = stud_id;
		this.branch = branch;
		this.first_year = first_year;
		this.second_year = second_year;
		this.third_year = third_year;
	}

	public StudentReport() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
