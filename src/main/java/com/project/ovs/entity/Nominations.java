package com.project.ovs.entity;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
@Table(name="nominations")
public class Nominations {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int nominateId;
	
	
	private String studid;
	
	@Column(name = "nominate_time", columnDefinition = "TIMESTAMP")
	private LocalDateTime localDateTime;
	
	@Column(name="status")
	private String status;   
	
	@Column(name="votes_count")
	private int votes_count;



	
}
