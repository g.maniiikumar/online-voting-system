package com.project.ovs.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;




import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name="student_info")
public class StudentInfo {
	
	@Id
	@Column(name="stud_id")
	private String studid;
	
	@Column(name="student_name")
	private String studentName;
	
	@Column(name="mobile")
	private String mobile;
	
	@Column(name="branch")
	private String branch;
	
	@Column(name="email")
	private String email;
	
	@Column(name="address")
	private String address;
	
	private String img_name;
	
	private String img_type;
	
	private String imag_filePath;
	
	@Transient
	private MultipartFile file;
	
	@Column(name="vote")
	private int vote;
	
	

}
