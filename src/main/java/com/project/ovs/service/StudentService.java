package com.project.ovs.service;


import java.util.List;

import org.springframework.http.ResponseEntity;

import com.project.ovs.dto.Result;
import com.project.ovs.dto.StudentReportResponse;
import com.project.ovs.entity.Nominations;
import com.project.ovs.entity.StudentInfo;

public interface StudentService {

	public StudentReportResponse getStudentReport(String studid);
	
	public String saveStudent(StudentInfo studentinfo);


	public List<Nominations> getSelectedNominations();
	public Result sendNomination(String studid);


	Object voteToSelectedStudent(String studpinno, String votepinno);

	public Result getVoteResult();
}
